﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public GameObject spark;
	public float damage;
	private GameObject allSparks;

	// Use this for initialization
	void Start () {
		allSparks = GameObject.FindGameObjectWithTag("AllSparks");
	}

	public void Hit(){
		GameObject newSpark = Instantiate(spark,allSparks.transform);
		newSpark.transform.position = this.gameObject.transform.position;
		Destroy(gameObject);
	}

	public float GetDamage(){
		return damage;
	}
}
