﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour{

	public AudioClip[] clips;
	public AudioClip bossMusic;
	public AudioClip WaringSound;
	public AudioClip levelPass;
	private AudioSource musicPlayer;

	void Awake(){
		musicPlayer = GetComponent<AudioSource>();
	}

	// Use this for initialization
	void Start(){
		DontDestroyOnLoad(this.gameObject);
	}

	public void PlaySceneMusic(){
		int level = SceneManager.GetActiveScene().buildIndex;
		if(clips[level]){
			musicPlayer.clip = clips[level];
			if(level != 0){
				musicPlayer.loop = true;
			} else{
				musicPlayer.loop = false;
			}
			musicPlayer.Play();
		}else{
			musicPlayer.Stop();
		}
	}

	public void PlayBossMusic(){
		musicPlayer.clip = bossMusic;
		musicPlayer.loop = true;
		musicPlayer.Play();
	}

	public void PlayWaringSound(){
		musicPlayer.clip = WaringSound;
		musicPlayer.loop = true;
		musicPlayer.Play();
	}

	public void PlayLevelPassMusic(){
		musicPlayer.Stop();
		musicPlayer.clip = levelPass;
		musicPlayer.loop = false;
		musicPlayer.Play();
	}

	private void StopSound(){
		musicPlayer.Stop();
	}
}
