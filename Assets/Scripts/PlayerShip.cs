﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShip : MonoBehaviour{

	public float speed;
	public float firingRate;
	public float shield;
	public GameObject playerLaserGreen;
	public float projectileSpeed;
	public AudioClip playerFireSound;
	public GameObject sparkEffect;
	private Text shieldNumber;
	private GameObject allSparks;
	private bool isFiring;
	private float gunHeat;
	private GameObject mainPlayPanel;
	private GameController gameController;


	// Use this for initialization
	void Start(){
		isFiring = false;
		shield = 200;
		gunHeat = 0;
		allSparks = GameObject.FindGameObjectWithTag("AllSparks");
		shieldNumber = GameObject.Find("ShieldNumber").GetComponent<Text>();
		mainPlayPanel = GameObject.Find("MainPlayPanel");
		gameController = GameObject.FindObjectOfType<GameController>();
	}
	
	// Update is called once per frame
	void Update(){
		if(isFiring){
			StartAttackMod();
		} else{
			StartDefendMod();
		}
		if(gunHeat > 0){
			gunHeat--;
		}
		shieldNumber.text = Mathf.CeilToInt(shield).ToString();

	}

	void OnTriggerEnter2D(Collider2D col){
		Projectile missile = col.gameObject.GetComponent<Projectile>();
		if(missile && (missile.tag == "EnemyFire")){
			missile.Hit();
			shield -= missile.GetDamage();
		} else if(col.gameObject.tag == "EnemyShip"){
			shield -= 50f;
		}
		if(shield < 0){
			GameObject spark = Instantiate(sparkEffect, allSparks.transform);
			spark.transform.position = gameObject.transform.position;
			mainPlayPanel.SetActive(false);
			gameController.PlayerShipDestroyed();
			Destroy(gameObject);
		}
	}

	public float GetSpeed(){
		return speed;
	}

	public void StartFiring(){
		isFiring = true;
	}

	public void StopFiring(){
		isFiring = false;
	}



	private void StartAttackMod(){
		if(gunHeat <= 0){
			Fire();
		}
	}

	private void StartDefendMod(){
		if(shield < 200){
			shield += 10f * Time.deltaTime;
		}
	}


	public void Fire(){
		Vector3 projectilePosition = gameObject.transform.position + new Vector3(0f, 0.5f, 0f);
		GameObject beam = Instantiate(playerLaserGreen, projectilePosition, Quaternion.identity) as GameObject;
		beam.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, projectileSpeed, 0f);
		AudioSource.PlayClipAtPoint(playerFireSound, projectilePosition);
		gunHeat += 1 / (Time.deltaTime * firingRate);
	}
}
