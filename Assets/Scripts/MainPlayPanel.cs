﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPlayPanel : MonoBehaviour{

	
	private GameObject playerShip;
	private Camera gameCamera;
	private Vector3 mousePosition;
	private Vector3 targetPosition;
	private float playerShipSpeed;
	private float newPositionX;
	private float newPositionY;

	// Use this for initialization
	void Start(){
		gameCamera = GameObject.FindObjectOfType<Camera>();
		playerShip = GameObject.FindGameObjectWithTag("PlayerShip");
		playerShipSpeed = playerShip.GetComponent<PlayerShip>().GetSpeed();
	}


	void OnMouseUp(){
		playerShip.GetComponent<PlayerShip>().StopFiring();
	}

	void OnMouseDrag(){
		playerShip.GetComponent<PlayerShip>().StartFiring();
		mousePosition = gameCamera.ScreenToWorldPoint(Input.mousePosition);
		targetPosition = new Vector3(Mathf.Clamp(mousePosition.x, 0.5f, 8.5f), Mathf.Clamp(mousePosition.y, 2f, 14f), -1f);
		GotoNewPosition();
	}

	void GotoNewPosition(){
		if(targetPosition.x > playerShip.transform.position.x + 0.2f){
			newPositionX = playerShip.transform.position.x + playerShipSpeed * Time.deltaTime;
		} else if(targetPosition.x < playerShip.transform.position.x - 0.2f){
			newPositionX = playerShip.transform.position.x - playerShipSpeed * Time.deltaTime;
		} else{
			newPositionX = playerShip.transform.position.x;
		}
		if(targetPosition.y > playerShip.transform.position.y + 0.2f){
			newPositionY = playerShip.transform.position.y + playerShipSpeed * Time.deltaTime;
		} else if(targetPosition.y < playerShip.transform.position.y - 0.2f){
			newPositionY = playerShip.transform.position.y - playerShipSpeed * Time.deltaTime;
		} else{
			newPositionY = playerShip.transform.position.y;
		}
		playerShip.transform.position = new Vector3(newPositionX, newPositionY, -1f);
	}
}
