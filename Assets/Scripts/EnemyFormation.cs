﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFormation : MonoBehaviour{

	public GameObject enemyPrefeb;
	public float width;
	public float height;
	public float speed;
	public float spawnDelay;
	public float respawnTimes;
	public bool moveLeft;
	private GameController gameController;
	private float xmin;
	private float xmax;
	// Use this for initialization
	void Start(){
		gameController = GameObject.FindObjectOfType<GameController>();
		//float distance = transform.position.z - Camera.main.transform.position.z;
		//Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
		//Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
		xmin = 0f + width / 2;
		xmax = 9f - width / 2;
		SpawnUntillFull();
	}
	
	// Update is called once per frame
	void Update(){
		if(moveLeft){
			transform.position += Vector3.left * speed * Time.deltaTime;
			if(transform.position.x <= xmin)
				moveLeft = false;
		} else{
			transform.position += Vector3.right * speed * Time.deltaTime;
			if(transform.position.x >= xmax)
				moveLeft = true;
		}

		if(AllMembersDead()){
			if(respawnTimes>0){
				respawnTimes--;
				SpawnUntillFull();
			}
			else{
				gameController.FormationDestroyed();
				Destroy(gameObject);
			}
		}
	}

	public void OnDrawGizmos(){
		Gizmos.DrawWireCube(transform.position, new Vector3(width, height));
	}

	Transform NextFreePosition(){
		foreach(Transform childPositionGameObject in transform){
			if(childPositionGameObject.childCount <= 0){
				return childPositionGameObject.transform;
			}
		}
		return null;
	}

	void SpawnUntillFull(){
		Transform freePosition = NextFreePosition();
		if(freePosition){
			GameObject enemy = Instantiate(enemyPrefeb, freePosition.position, Quaternion.identity) as GameObject;
			enemy.transform.parent = freePosition;
			Invoke("SpawnUntillFull", spawnDelay);
		}
	}

	bool AllMembersDead(){
		foreach(Transform childPositionGameObject in transform){
			if(childPositionGameObject.childCount > 0){
				return false;
			}
		}
		return true;
	}

}
