﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sparks : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke("BurnOut",1f);
	}
	
	private void BurnOut(){
		Destroy(gameObject);
	}
}
