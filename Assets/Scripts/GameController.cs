﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour{

	public static int score;
	public int levelEnemyFormations;
	public GameObject formationSquare;
	public GameObject formationAssult;
	public GameObject formationDefend;
	public GameObject formationBoss;
	private MusicManager musicManager;
	private LevelManager levelManager;
	private Text scoreText;
	private GameObject warningMessage;
	private GameObject winMessage;

	void Awake(){
		musicManager = GameObject.FindObjectOfType<MusicManager>();
	}

	// Use this for initialization
	void Start(){
		musicManager.PlaySceneMusic();
		levelManager = GameObject.FindObjectOfType<LevelManager>();
		if(SceneManager.GetActiveScene().buildIndex > 2){
			scoreText = GameObject.Find("ScoreNumber").GetComponent<Text>();
			if(SceneManager.GetActiveScene().name=="Win"||SceneManager.GetActiveScene().name=="Lose"){
				scoreText.text = score.ToString();
			}else{
				warningMessage = GameObject.Find("WarningMessage");
				warningMessage.SetActive(false);
				winMessage = GameObject.Find("WinMessage");
				winMessage.SetActive(false);
				Instantiate(formationSquare, gameObject.transform);
			}

		}

	}

	public void FormationDestroyed(){
		if(levelEnemyFormations >= 2){
			AddFormation();
			levelEnemyFormations--;
		} else if(levelEnemyFormations == 1){
			AddBoss();
			levelEnemyFormations--;
		} else{
			winMessage.SetActive(true);
			Invoke("LevelPass",4f);
		}
	}

	public void PlayerShipDestroyed(){
		Invoke("Defeated",3f);
	}

	private void Defeated(){
		levelManager.LoadLose();
	}

	public void IncreaseScore(int number){
		score += number;
		scoreText.text = score.ToString();
	}

	private void AddFormation(){
		int chance = Random.Range(0, 100);
		if(chance >= 90){
			Instantiate(formationSquare, gameObject.transform);
			Instantiate(formationAssult, gameObject.transform);
			Instantiate(formationDefend, gameObject.transform);
		} else if(chance >= 60){
			Instantiate(formationSquare, gameObject.transform);
		} else if(chance >= 30){
			Instantiate(formationAssult, gameObject.transform);
		} else{
			Instantiate(formationDefend, gameObject.transform);
		}
	}

	private void AddBoss(){
		musicManager.PlayWaringSound();
		warningMessage.SetActive(true);
		Invoke("CloseWarningMessage",3f);
		Invoke("BossAppear", 4f);
	}

	private void BossAppear(){
		musicManager.PlayBossMusic();
		Instantiate(formationBoss, gameObject.transform);
	}

	private void CloseWarningMessage(){
		warningMessage.SetActive(false);
	}

	private void LevelPass(){
		musicManager.PlayLevelPassMusic();
		levelManager.LoadNextLevel();
	}
}
