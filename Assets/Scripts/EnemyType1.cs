﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyType1 : MonoBehaviour{

	public GameObject enemyLaser;
	public float health;
	public float projectileSpeed;
	public float fireingChance;
	public int score;
	public AudioClip spawnSound;
	public GameObject sparkEffect;
	private GameController gameController;
	private GameObject allSparks;

	void Start(){
		AudioSource.PlayClipAtPoint(spawnSound, gameObject.GetComponentInParent<Transform>().position);
		allSparks = GameObject.FindGameObjectWithTag("AllSparks");
		gameController = GameObject.FindObjectOfType<GameController>();
	}

	void Update(){
		if(Random.Range(0f, 100f) < fireingChance * Time.deltaTime){
			Fire();
		}
	}

	void OnTriggerEnter2D(Collider2D col){
		Projectile missile = col.gameObject.GetComponent<Projectile>();
		if(missile && (missile.tag == "PlayerFire")){
			missile.Hit();
			health -= missile.GetDamage();
		} else if(col.gameObject.tag == "PlayerShip"){
			health -= 100f;
		}
		if(health <= 0){
			GameObject spark = Instantiate(sparkEffect, allSparks.transform);
			spark.transform.position = gameObject.transform.position;
			gameController.IncreaseScore(score);
			Destroy(gameObject);
		}
	}

	void Fire(){
		Vector3 projectilePosition = transform.position - new Vector3(0f, 0.5f, 0f);
		GameObject beam = Instantiate(enemyLaser, projectilePosition, Quaternion.identity) as GameObject;
		beam.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, projectileSpeed, 0f);
	}
}
