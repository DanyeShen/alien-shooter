﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour{

	// Use this for initialization
	void Start(){
		if(SceneManager.GetActiveScene().buildIndex == 0){
			Invoke("LoadStart",3f);
		}
	}
	
	// Update is called once per frame
	void Update(){
		
	}

	public void LoadStart(){
		SceneManager.LoadScene("01_StartUI");
	}

	public void LoadWin(){
		SceneManager.LoadScene("Win");
	}

	public void LoadLose(){
		SceneManager.LoadScene("Lose");
	}

	public void LoadNextLevel(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
	}

	public void LoadOptions(){
		SceneManager.LoadScene("Options");
	}

	public void Quit(){
		Application.Quit();
	}
}
